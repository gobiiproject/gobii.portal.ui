// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');
const appUrl = 'http://localhost:4200/';

exports.config = {
  allScriptsTimeout: 20000,
  specs: [
    './e2e/**/*.e2e-spec.ts'
  ],
  capabilities: {
    'browserName': 'chrome'
  },
  directConnect: true,
  baseUrl: appUrl,
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 20000,
    print: function() {}
  },
  onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));

    // log in before we start the tests
    browser.driver.get(appUrl + 'login');
    browser.driver.findElement(by.name('username')).sendKeys('sentst');
    browser.driver.findElement(by.name('password')).sendKeys('fake');
    browser.driver.findElement(by.css('[class="btn btn-primary"]')).click();

    return browser.driver.wait(function() {
      return browser.driver.getCurrentUrl().then(function(url) {
        return /feature/.test(url);
      });
    }, 20000);
  }
};
