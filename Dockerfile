FROM node:10.15.1 AS build-env

ENV TERM=xterm
ENV ROOT /var/www/app

COPY package*.json $ROOT/
WORKDIR $ROOT
RUN npm ci
COPY . .
RUN npm run build

#------------------------------------------------------

FROM nginx:1.13.6-alpine
ENV ROOT /var/www/app

RUN apk add --no-cache gettext

WORKDIR /var/www/build
COPY --from=build-env $ROOT/build ./

WORKDIR /var/www
COPY nginx.conf.template ./
COPY run.sh ./
COPY env.js.template ./
RUN chmod +x ./run.sh

# start sever
CMD ./run.sh
