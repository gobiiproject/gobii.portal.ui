import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  user: any = { name: 'Demo User', user: 'demo_user' };

  constructor(
    ) {}

  ngOnInit() {
  }
}
