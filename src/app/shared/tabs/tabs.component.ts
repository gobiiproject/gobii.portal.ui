import { Component, Input, OnInit } from '@angular/core';
import { NavLinks } from '../../models/nav-link.model';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {
  @Input() dynamicTabs: Array<NavLinks>;

  constructor() {
   }

  ngOnInit() {
  }

}
