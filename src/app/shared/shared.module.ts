import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxLibCommonModule } from 'ngx-lib-common';
import { NgxLibFormlyModule } from 'ngx-lib-formly';
import { NgxLibMaterialModule } from 'ngx-lib-material';
import { NgxLibStylesModule } from 'ngx-lib-styles';
import { NgxPermissionsModule } from 'ngx-permissions';
import { TabsComponent } from './tabs/tabs.component';
import { DataTableComponent } from './data-table/data-table.component';
import { MatRippleModule } from '@angular/material/core';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgxPermissionsModule,
    // Common libraries
    NgxLibCommonModule,
    NgxLibStylesModule,
    NgxLibMaterialModule,
    NgxLibFormlyModule,
    // To be added to ngx lib
    MatRippleModule
  ],
  exports: [
    CommonModule,
    RouterModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgxLibCommonModule,
    NgxLibStylesModule,
    NgxLibMaterialModule,
    NgxLibFormlyModule,
    TabsComponent,
    DataTableComponent,
    MatRippleModule
  ],
  declarations: [
    TabsComponent,
    DataTableComponent
  ]
})
export class SharedModule { }
