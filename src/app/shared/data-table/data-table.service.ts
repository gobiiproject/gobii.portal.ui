import { Injectable } from '@angular/core';
import { TableQueryParams } from '../../models/table-query-params.model';
import { ActivatedRouteSnapshot } from '@angular/router';
import { TableDefinition } from '../../models/table-definition.model';

@Injectable()
export class DataTableService {
  constructor() {}

  getQueryParams(
    tableDefinition: TableDefinition,
    route: ActivatedRouteSnapshot
  ): TableQueryParams {
    return {
      ...route.queryParams,
      sort:
        route.queryParams.sort ||
        `${tableDefinition.sortActive},${tableDefinition.sortDirection}`
    };
  }
}
