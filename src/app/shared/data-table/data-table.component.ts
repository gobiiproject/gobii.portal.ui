import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { MatSort, MatPaginator, PageEvent, Sort } from '@angular/material';
import { get } from 'lodash';

import * as DateFormats from '../../constants/date-formats.constants';
import { Page } from '../../models/page.model';
import { ColumnDefinition } from '../../models/column-definition.model';
import { TableDefinition } from '../../models/table-definition.model';


@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
})
export class DataTableComponent implements OnInit {
  private _actions;

  @Input() tableDefinition: TableDefinition;
  @Input() selectable: boolean;
  @Input() hasActions: boolean;
  @Input() data: any[];
  @Input() page: Page;
  @Input() dateFormat: string = DateFormats.shortDate;
  @Input() dateTimeFormat: string = DateFormats.shortDateTime;
  @Input() readOnly: boolean;
  @Output() selectRow: EventEmitter<any> = new EventEmitter();
  @Output() checkboxSelect: EventEmitter<any> = new EventEmitter();
  @Output() deleteRow: EventEmitter<any> = new EventEmitter();
  @Output() pageChange: EventEmitter<PageEvent> = new EventEmitter();
  @Output() sortChange: EventEmitter<Sort> = new EventEmitter();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get actions() {
    return this._actions;
  }

  @Input()
  set actions(actionConfig: object) {
    this._actions = Object.keys(actionConfig).map((action: string) => ({
      name: action,
      onClickHandler: actionConfig[action]
    }));
  }

  displayedColumns: string[];
  columns: ColumnDefinition[];
  sortDirection: string;
  sortActive: string;
  tableSortDisabled: boolean = false;

  constructor() { }

  ngOnInit(): void {
    this.loadTableDefinition();
    this.paginator.page.subscribe(page => this.pageChange.emit(page));
    this.sort.sortChange.subscribe(sort => {
      this.resetPaging();
      this.sortChange.emit(sort);
    });
  }

  resetPaging = () => (this.paginator.pageIndex = 0);

  onClickRow(row): void {
    this.selectRow.emit(row);
  }

  onDeleteRow(event: Event, row): void {
    event.stopPropagation();
    this.deleteRow.emit(row);
  }

  onCheckboxSelect(row, columnDef): void {
    const record: any = { ...row };
    record[columnDef] = !row[columnDef];
    this.checkboxSelect.emit(record);
  }

  onClickAction(row, action, event) {
    event.stopPropagation();
    action.onClickHandler(row);
  }

  loadTableDefinition(): void {
    this.columns = this.tableDefinition.columns;
    this.displayedColumns = this.selectDisplayedColumns(this.columns);
    this.sortActive = this.tableDefinition.sortActive;
    this.sortDirection = this.tableDefinition.sortDirection;
    this.tableSortDisabled = this.tableDefinition.tableSortDisabled;
  }

  selectDisplayedColumns(columns: ColumnDefinition[]): string[] {
    let displayedColumns = columns.map(x => x.columnDef);
    if (this.hasActions) {
      displayedColumns = [...displayedColumns, 'actions'];
    }
    return displayedColumns;
  }

  renderCellValue = (row: any, columnDef: string): string =>
    get(row, columnDef)
}

