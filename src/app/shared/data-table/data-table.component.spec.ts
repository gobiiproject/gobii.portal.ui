import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { DataTableComponent } from './data-table.component';
import {
  MatTableModule,
  MatSortModule,
  MatPaginatorModule
} from '@angular/material';
// import { MockStore } from '../../../../../kup.management.ui/src/app/util/testing/test-helper';
// import { Store } from '@ngrx/store';
import { DataTableService } from './data-table.service';

describe('DataTableComponent', () => {
  let component: DataTableComponent;
  let fixture: ComponentFixture<DataTableComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [MatTableModule, MatPaginatorModule, MatSortModule],
        declarations: [DataTableComponent],
        providers: [
          // { provide: Store, useClass: MockStore },
          { provide: DataTableService, useValue: {} }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
