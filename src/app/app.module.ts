import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { DashboardModule } from '../dashboard/dashboard.module';
import { JobStatusModule } from '../job-status/job-status.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  declarations: [AppComponent],
  imports: [
    CoreModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    NgxPermissionsModule.forRoot(),
    // Eager Loaded Features
    DashboardModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
