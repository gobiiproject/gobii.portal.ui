export const permissions: any = [
    {value: 'Admin', label: 'Admin'},
    {value: 'Curator', label: 'Curator'},
    {value: 'User', label: 'User'},
];
