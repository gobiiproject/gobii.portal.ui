export const CROPS: any = [
    {
        value: 'Wheat',
        label: 'Wheat'
    },
    {
        value: 'Barley',
        label: 'Barley'
    },
    {
        value: 'Potato',
        label: 'Potato'
    },
    {
        value: 'Apple',
        label: 'Apple'
    },
    {
        value: 'Kiwifruit',
        label: 'Kiwifruit'
    }
];
