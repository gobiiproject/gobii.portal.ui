import { TableDefinition } from '../models/table-definition.model';

export class TableDefinitionFactory {
    private constructor() { }
    static jobList(): TableDefinition {
        return {
            sortActive: 'dateTime',
            sortDirection: 'asc',
            tableSortDisabled: true,
            columns: [
                {
                    columnDef: 'dateTime',
                    sortKey: 'dateTime',
                    header: 'Date and Time',
                    formatter: 'DateTime',
                    sortDisabled: true,
                },
                {
                    columnDef: 'id',
                    sortKey: 'id',
                    header: 'ID',
                    formatter: 'Text',
                    sortDisabled: true,
                },
                {
                    columnDef: 'type',
                    sortKey: 'type',
                    header: 'Type',
                    formatter: 'Text',
                    sortDisabled: true,
                },
                {
                    columnDef: 'createdBy',
                    sortKey: 'createdBy',
                    header: 'User',
                    formatter: 'Text',
                    sortDisabled: true,
                },
                {
                    columnDef: 'status',
                    sortKey: 'status',
                    header: 'Status',
                    formatter: 'Status',
                    sortDisabled: true,
                }
              ]
        };
    }
    static manageAppsList(): TableDefinition {
        return {
            sortActive: 'Name',
            sortDirection: 'asc',
            tableSortDisabled: true,
            columns: [
                {
                    columnDef: 'imgUrl',
                    sortKey: 'imgUrl',
                    header: 'App',
                    formatter: 'ImageIcon',
                    sortDisabled: true,
                },
                {
                    columnDef: 'name',
                    sortKey: 'name',
                    header: 'Name',
                    formatter: 'Text',
                    sortDisabled: true,
                },
                {
                    columnDef: 'description',
                    sortKey: 'description',
                    header: 'Description',
                    formatter: 'Text',
                    sortDisabled: true,
                },
                {
                    columnDef: 'permission',
                    sortKey: 'permission',
                    header: 'Access',
                    formatter: 'Text',
                    sortDisabled: true,
                }
              ]
        };
    }
    static job(): TableDefinition {
        return {
            sortActive: 'Name',
            sortDirection: 'asc',
            tableSortDisabled: true,
            columns: [
                {
                    columnDef: 'identifier',
                    sortKey: 'identifier',
                    header: 'Identifier',
                    formatter: 'Text',
                    sortDisabled: true,
                },
                {
                    columnDef: 'name',
                    sortKey: 'name',
                    header: 'Name',
                    formatter: 'Text',
                    sortDisabled: true,
                },
                {
                    columnDef: 'id',
                    sortKey: 'id',
                    header: 'Id',
                    formatter: 'Text',
                    sortDisabled: true,
                }
              ]
        };
    }

    static data(): TableDefinition {
        return {
            sortActive: 'Table',
            sortDirection: 'asc',
            tableSortDisabled: true,
            columns: [
                {
                    columnDef: 'table',
                    sortKey: 'table',
                    header: 'Table',
                    formatter: 'Text',
                    sortDisabled: true,
                },
                {
                    columnDef: 'total',
                    sortKey: 'total',
                    header: 'Total',
                    formatter: 'Text',
                    sortDisabled: true,
                },

              ]
        };
    }

    static file(): TableDefinition {
        return {
            sortActive: 'Type',
            sortDirection: 'asc',
            tableSortDisabled: true,
            columns: [
                {
                    columnDef: 'type',
                    sortKey: 'type',
                    header: 'Type',
                    formatter: 'Text',
                    sortDisabled: true,
                },
                {
                    columnDef: 'path',
                    sortKey: 'path',
                    header: 'Path',
                    formatter: 'Text',
                    sortDisabled: true,
                },
                {
                    columnDef: 'size',
                    sortKey: 'size',
                    header: 'Size',
                    formatter: 'Text',
                    sortDisabled: true,
                }
              ]
        };
    }
}
