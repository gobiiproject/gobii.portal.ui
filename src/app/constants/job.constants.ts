export enum JobType {
  LOAD = 'LOAD',
  EXTRACT = 'EXTRACT'
}

export enum JobStatus {
  COMPLETED = 'COMPLETED',
  FAILED = 'FAILED',
  SUMITTIED = 'SUMITTIED',
  DIGESTING = 'DIGESTING',
  TRANSFORMING = 'TRANSFORMING',
  VALIDATING = 'VALIDATING',
  LOADING = 'LOADING',
  QCING = 'QCING',
  EXTRACTMETADATA = 'EXTRACTING METADATA',
  EXTRACTGENOTYPES = 'EXTRACTING GENOTYPES',
  FORMATTING = 'FORMATTING'
}

export const JOBITEMSTATUS: any = [
  {
    value: 'COMPLETED',
    label: 'Completed'
  },
  {
    value: 'FAILED',
    label: 'Failed'
  },
  {
    value: 'INPROGRESS',
    label: 'In Progress'
  }
];

export const JOBLOADPROCESS: any = [
  {
    value: 'SUMITTIED',
    label: 'Sumitted'
  },
  {
    value: 'DIGESTING',
    label: 'Digesting'
  },
  {
    value: 'TRANSFORMING',
    label: 'Transforming'
  },
  {
    value: 'VALIDATING',
    label: 'Validating'
  },
  {
    value: 'LOADING',
    label: 'Loading'
  },
  {
    value: 'QCING',
    label: 'Qcing'
  },
  {
    value: 'COMPLETED',
    label: 'Completed'
  }
];

export const JOBEXTRACTPROCESS: any = [
  {
    value: 'SUMITTIED',
    label: 'Sumitted'
  },
  {
    value: 'EXTRACTMETADATA',
    label: 'Extracting Metadata'
  },
  {
    value: 'EXTRACTGENOTYPES',
    label: 'Extracting Genotypes'
  },
  {
    value: 'FORMATTING',
    label: 'Formatting'
  },
  {
    value: 'QCING',
    label: 'Qcing'
  },
  {
    value: 'COMPLETED',
    label: 'Completed'
  }
];
