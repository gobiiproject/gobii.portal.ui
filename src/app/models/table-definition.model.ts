import { ColumnDefinition } from './column-definition.model';

export interface TableDefinition {
  sortActive: string;
  sortDirection: string;
  tableSortDisabled?: boolean;
  columns: ColumnDefinition[];
}

export interface TableDefinitions {
  [key: string]: TableDefinition;
}
