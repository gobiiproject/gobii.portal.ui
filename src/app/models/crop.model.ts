export interface Crop {
    label?: string;
    name?: string;
    imgUrl?: string;
    apps?: App[];
    id?: string;
}

export interface App {
    label?: string;
    name?: string;
    id?: number;
    appUrl?: string;
    description?: string;
    imgUrl?: string;
    permission?: string;
    version?: string;
}
