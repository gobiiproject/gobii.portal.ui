export interface ColumnDefinition {
  columnDef: string;
  header: string;
  formatter?: string;
  sortKey?: string; // Used as <mat-table [mat-sort-header] />
  tooltipKey?: string;
  sortDisabled?: boolean;
  disabled?: boolean;
  fixed?: boolean;
}
