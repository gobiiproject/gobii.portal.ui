export interface TableQueryParams {
  match?: string;
  page?: string;
  size?: string;
  sort?: string;
  [key: string]: string;
}
