# KDP Template Project Source

## How to build an application from this template

### Models

The data model classes that your application is based on go in the `models` directory. They represent the objects that you will be getting from your data source(s).

#### Example

```TypeScript
export class Vine {
  code: string;
  location: Location;

  constructor(code, location) {
    this.code = code;
    this.location = location;
  }
}
```

### Features

Each feature of the application has its own directory, which can then be subdivided into groups of components and services, if necessary.

### Shared

Services and components that are used by different features go in the `shared` directory. Remember that the less that's shared, the easier the program is to maintain. Do everyone a favour and keep this directory small.

### Store

The store is the central repository of application state. It goes in its own directory.

There's a lot that goes along with using a store, so below is a brief description of what to do. More details can be found in [a gist by btroncone](https://gist.github.com/btroncone/a6e4347326749f938510).

We set data in the store by dispatching actions to it. We get data from the store by selecting from it, using reducers.

The store is injected into our components. We take what we want from it by naming a reducer. The information is returned as an observable, so we want to subscribe to it like this:

```TypeScript
export class Component {
  vine: Vine;

  // this lets us manage our subscriptions
  // and unsubscribe when the component
  // is destroyed, so we don't get memory leaks
  // memory leaks are bad news
  private unsubscribe: Subject<void>;

  constructor(private store: Store) {
    this.store.select('vine')
      .takeUntil(this.unsubscribe)
      .subscribe(vine => {
        this.vine = vine;
      }));
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
```

#### Actions

An action is an object that tells us what we want to do to the store's data. It's a string telling us what to do, along with any data that we might need to do it.

##### Example

```TypeScript
// actions are given types like this
export const LOG_IN_USER = 'Log In User';
export const CHANGE_VINE = 'Change Vine';
export const MOVE_VINE = 'Move Vine';

export class Action {
  public type: string;
  public payload: any;

  constructor(type: string, payload?: any) {
    this.type = type;
    this.payload = payload;
  }
}

// using an action
let dbLocation: Location = new Location('T33-01-01b');
let dbVine: Vine = new Vine('T08.33-01-01b', dbLocation);

let changeVineAction = new Action(CHANGE_VINE, dbVine);
```

#### Reducers

A reducer is a function that takes in a state (a model class or other piece of application-wide data) and an action and returns a copy of the state that incorporates any changes that the action wants made.

The map of reducers that is given to the application module (in app.module) defines the available selectors for the store (the options we have when we call `store.select`). Its contents correspond to the contents of the store and each reducer returns an object of the same type as that found in the store.

##### Important Note

It's important that a reducer NEVER changes the state that is given to it. That state is used to trace changes in the application, so we must copy what we want into a new state and return that, not reassign properties in the state we're given.

##### Example

```TypeScript
export class AppState {
  vine: Vine;
  locations: Location[];
}

export const reducers = {
  vine: vineReducer,
  locations: locationReducer
};

export function vineReducer(
    state: Vine = initialVine,
    action: actions.Action) {
  switch (action.type) {
    case actions.CHANGE_VINE:
      return Object.assign(Vine, action.payload);
    case actions.MOVE_VINE:
      return Object.assign(Vine, action.payload);
    default:
      return state;
  }
}
```

When an application has too many reducers for one file, they can be split into separate directories (or modules) based on features.
