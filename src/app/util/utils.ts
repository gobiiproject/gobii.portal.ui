export function isEmptyObject(object): boolean {
  return Object.keys(object).length === 0;
}

export function isUndefinedObject(object): boolean {
  return typeof object === 'undefined';
}

export function isNullOrUndefined(elem): boolean {
  return elem === 'undefined' || elem === null;
}
