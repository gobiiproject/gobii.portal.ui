import { ActivatedRoute } from '@angular/router';
import { JobStatusService } from './../../services/job-status.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { TableDefinition } from '../../../app/models/table-definition.model';
import { TableDefinitionFactory } from '../../../app/constants/table-definition-factory';
import { Job } from '../../models/job.model';
import { Observable, of, Subscription } from 'rxjs';
import { Data } from '../../models/data.model';
import { FileInfo } from '../../models/file-info.model';

@Component({
  selector: 'app-job-item',
  templateUrl: './job-item.component.html',
  styleUrls: ['./job-item.component.scss']
})
export class JobItemComponent implements OnInit, OnDestroy {

  readonly jobTableDefinition: TableDefinition = TableDefinitionFactory.job();
  readonly dataTableDefinition: TableDefinition = TableDefinitionFactory.data();
  readonly fileTableDefinition: TableDefinition = TableDefinitionFactory.file();
  jobItemStatus: any;
  summaryJob$: Observable<Job[]>;
  summaryData$: Observable<Data[]>;
  summaryFile$: Observable<FileInfo[]>;
  summaryError: string;
  subscription = new Subscription();
  constructor(private service: JobStatusService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadJobItemStatus();
  }

  loadJobItemStatus() {
    const id = +(this.getParmId());
    this.subscription.add(this.service.getItemStatus(id).subscribe((response) => {
      this.jobItemStatus = response;
      this.summaryJob$ = of(response.summary.job);
      this.summaryData$ = of(response.summary.data);
      this.summaryFile$ = of(response.summary.file);
      this.summaryError = response.summary.error;

    }));
  }

  getParmId() {
    return this.activatedRoute.snapshot.params.id;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
