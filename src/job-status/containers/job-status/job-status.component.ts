import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { JobStatusService } from '../../services/job-status.service';
import { TableDefinitionFactory } from '../../../app/constants/table-definition-factory';
import { TableDefinition } from '../../../app/models/table-definition.model';
import { JobList } from '../../models/job-list.model';
import { browser } from 'protractor';

@Component({
  selector: 'app-job-status',
  templateUrl: './job-status.component.html',
  styleUrls: ['./job-status.component.scss']
})
export class JobStatusComponent implements OnInit {
  readonly tableDefinition: TableDefinition = TableDefinitionFactory.jobList();
  jobs$: Observable<JobList[]>;

  constructor(
     private service: JobStatusService,
     private router: Router,
     private activatedRoute: ActivatedRoute
     ) { }

    ngOnInit() {
      this.loadJobs();
    }

    loadJobs() {
      this.jobs$ = this.service.getJobList();
    }

    onViewItem = (job: JobList) => {
      console.log('viewItem');
      this.router.navigate([`${job.id}`], {
        relativeTo: this.activatedRoute
      });
    }

}
