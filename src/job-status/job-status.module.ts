import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../app/shared/shared.module';
import { JobStatusComponent } from './containers/job-status/job-status.component';
import { JobStatusRoutingModule } from './job-status-routing.module';
import { JobItemComponent } from './containers/job-item/job-item.component';
import { JobProgressComponent } from './components/job-progress/job-progress.component';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    JobStatusRoutingModule
  ],
  declarations: [
    JobStatusComponent,
    JobItemComponent,
    JobProgressComponent,
],
providers: [
  {
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: { displayDefaultIndicatorType: false }
  }
]
})
export class JobStatusModule { }
