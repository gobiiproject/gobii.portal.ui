import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatStepper } from '@angular/material';

@Component({
  selector: 'app-job-progress',
  templateUrl: './job-progress.component.html',
  styleUrls: ['./job-progress.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class JobProgressComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  @Input() jobStatus: any;
  jobType: string;
  progress: any[];
  jobName: string;
  submitState: string;
  metadataState: string;
  genotypeState: string;
  formattingState: string;
  qcingState: string;
  completedState: string;
  digestingState: string;
  transformingState: string;
  validatingState: string;
  loadingState: string;
  constructor() { }

  ngOnInit() {
    this.loadJobStatus();
  }

  loadJobStatus() {
    this.progress = this.jobStatus.progress;
    this.jobType = this.jobStatus.type;
    this.jobName = this.jobStatus.name;
    this.stepState();
  }

  checkJobType() {
    if (this.jobType === 'EXTRACT') {
      return true;
    } else {
      return false;
    }
  }
  stepState() {
    this.progress.forEach(element => {
      for (const key in element) {
        if (key === 'submitted') {
          this.submitState = this.state(element, key);
        } else if (key === 'extractingMetadata') {
          this.metadataState = this.state(element, key);
        } else if (key === 'extractingGenotypes') {
          this.genotypeState = this.state(element, key);
        } else if (key === 'formatting') {
          this.formattingState = this.state(element, key);
        } else if (key === 'qcing') {
          this.qcingState = this.state(element, key);
        } else if (key === 'completed') {
          this.completedState = this.state(element, key);
        } else if (key === 'digesting') {
          this.digestingState = this.state(element, key);
        } else if (key === 'transforming') {
          this.transformingState = this.state(element, key);
        } else if (key === 'validating') {
          this.validatingState = this.state(element, key);
        } else if (key === 'loading') {
          this.loadingState = this.state(element, key);
        }
      }
    });
  }

  state(element, key): string {
    if (element[key] === 'failed') {
      return 'error';
    } else if (element[key] === 'inprogress') {
      return 'more_horiz';
    } else if (element[key] === 'done') {
      return 'done';
    } else {
      return 'panorama_fish_eye';
    }
  }
}
