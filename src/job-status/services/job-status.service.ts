import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { baseUrl } from '../../app/constants/path.constants';
import { JobList } from '../models/job-list.model';

@Injectable({
  providedIn: 'root'
})
export class JobStatusService {
  jobListEndpoint = './../../assets/jobs.json';
  jobItemEndpoint = './../../assets/job-items.json';

  api = baseUrl;
  constructor(private http: HttpClient) { }

  getJobList(): Observable<JobList[]> {
    return this.http.get(this.jobListEndpoint).pipe(
      map((response: any) => response.jobs),
      catchError((error: any) => throwError(error))
    );
  }

  getItemStatus(id: number) {
    return this.http.get(this.jobItemEndpoint).pipe(
      map((response: any) => {
          const jobItem = response.find(e => e.id === id);
          return jobItem;
      }
      ),
      catchError((error: any) => throwError(error))
    );
  }


}
