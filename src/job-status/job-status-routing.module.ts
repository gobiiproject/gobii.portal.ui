import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobStatusComponent } from './containers/job-status/job-status.component';
import { JobItemComponent } from './containers/job-item/job-item.component';

const routes: Routes = [
  {
    path: '',
    component: JobStatusComponent,
  },
  {
    path: ':id',
    component: JobItemComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobStatusRoutingModule { }
