import { JobType, JobStatus } from '../../app/constants/job.constants';

export interface JobList {
    id: number;
    type: JobType;
    createdBy: string;
    dateTime: Date;
    status: JobStatus;
}
