export interface FileInfo {
    path: string;
    size: string;
    type: string;
}
