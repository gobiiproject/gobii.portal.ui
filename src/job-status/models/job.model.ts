export interface Job {
    id: string;
    identifier: string;
    name: string;
}
