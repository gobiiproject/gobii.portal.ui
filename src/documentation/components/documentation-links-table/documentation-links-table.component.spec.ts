import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentationLinksTableComponent } from './documentation-links-table.component';

describe('DocumentationLinksTableComponent', () => {
  let component: DocumentationLinksTableComponent;
  let fixture: ComponentFixture<DocumentationLinksTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentationLinksTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentationLinksTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
