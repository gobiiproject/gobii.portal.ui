import { Component, Input, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { MatTableDataSource } from '@angular/material';
import { Documentation } from '../../models/documentation.model';

@Component({
  selector: 'app-documentation-links-table',
  templateUrl: './documentation-links-table.component.html',
  styleUrls: ['./documentation-links-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class DocumentationLinksTableComponent {
  displayedColumns = ['logo', 'name', 'description'];
  expandedElement: Documentation | null;
  readonly dataSource = new MatTableDataSource<any>();

  @Input()
  get data(): any[] {
    return this.dataSource.data;
  }

  set data(value: any[]) {
    if (value === undefined) {
      value = [];
    }
    this.dataSource.data = value;
  }

  openLink(url: string) {
    window.open(url, '_blank');
  }
}
