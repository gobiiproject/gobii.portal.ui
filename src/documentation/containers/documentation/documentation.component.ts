import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { DocumentationService } from '../../services/documentation.service';

@Component({
  selector: 'app-documentation',
  templateUrl: './documentation.component.html',
  styleUrls: ['./documentation.component.scss']
})
export class DocumentationComponent implements OnInit {
  links$: Observable<any>;

  constructor(private service: DocumentationService) { }

  ngOnInit() {
    this.loadDocumentationLinks();
  }

  loadDocumentationLinks() {
    this.links$ = this.service.getDocumentationLinks();
  }

}
