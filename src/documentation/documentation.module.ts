import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocumentationRoutingModule } from './documentation-routing.module';
import { DocumentationComponent } from './containers/documentation/documentation.component';
import { DocumentationLinksTableComponent } from './components/documentation-links-table/documentation-links-table.component';
import { SharedModule } from '../app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DocumentationRoutingModule
  ],
  declarations: [DocumentationComponent, DocumentationLinksTableComponent]
})
export class DocumentationModule { }
