import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { baseUrl } from './../../app/constants/path.constants';

@Injectable({
  providedIn: 'root'
})
export class DocumentationService {
  api = baseUrl;
  constructor(private http: HttpClient) { }

  getDocumentationLinks(): Observable<any> {
    return this.http.get('./../../assets/documentation-links.json').pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }
}
