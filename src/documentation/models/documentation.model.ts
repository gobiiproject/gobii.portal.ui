export interface Documentation {
    name: string;
    description: string;
    docs: Link[];
}

export interface Link {
    name: string;
    url: string;
    id: string;
}
