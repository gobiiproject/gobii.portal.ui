import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './containers/dashboard/dashboard.component';

const subRoutes: Routes = [
  { path: '', redirectTo: 'applications/web', pathMatch: 'full' },
  {
    path: 'applications',
    loadChildren: './../applications/applications.module#ApplicationsModule'
  },
  {
    path: 'documentation',
    loadChildren: './../documentation/documentation.module#DocumentationModule'
  },
  {
    path: 'manage-apps',
    loadChildren: './../manage-apps/manage-apps.module#ManageAppsModule'
  }
];

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: subRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
