import { Component, OnInit } from '@angular/core';
import { NavLink } from '../../../app/models/nav-link.model';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  tabs: Array<NavLink> = [
    {
      label: 'Web Applications',
      path: 'applications/web'
    },
    {
      label: 'Desktop Applications',
      path: 'applications/desktop'
    },
    {
      label: 'Documentation',
      path: 'documentation',
    },
    {
      label: 'Manage Application Links',
      path: 'manage-apps'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
