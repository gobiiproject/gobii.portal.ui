import { WebApplicationsComponent } from './container/web-applications/web-applications.component';
import { DesktopApplicationsComponent } from './container/desktop-applications/desktop-applications.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'web',
    component: WebApplicationsComponent
  },
  {
    path: 'desktop',
    component: DesktopApplicationsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule { }
