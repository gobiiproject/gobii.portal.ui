import { CropsDropdownComponent } from './components/crops-dropdown/crops-dropdown.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationsRoutingModule } from './applications-routing.module';
import { ApplicationListComponent } from './components/application-list/application-list.component';
import { WebApplicationsComponent } from './container/web-applications/web-applications.component';
import { DesktopApplicationsComponent } from './container/desktop-applications/desktop-applications.component';
import { SharedModule } from '../app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ApplicationsRoutingModule
  ],
  declarations: [
    ApplicationListComponent,
    WebApplicationsComponent,
    DesktopApplicationsComponent,
    CropsDropdownComponent]
})
export class ApplicationsModule { }
