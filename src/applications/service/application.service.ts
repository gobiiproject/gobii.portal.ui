import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {
  webApplicationsEndpoint = '../../assets/web-applications.json';
  desktopApplicationsEndpoint = '../../assets/desktop-applications.json';
  cropsEndpoint = '../../assets/crops.json';

  constructor(private http: HttpClient) { }

  // Note: Not being used anywhere for now.
  getCrops(): Observable<any[]> {
    return this.http.get(this.cropsEndpoint).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getApplications(endpoint): Observable<any> {
    return this.http.get(endpoint).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getWebApps(): Observable<any> {
    return this.getApplications(this.webApplicationsEndpoint);
  }

  getDesktopApps(): Observable<any> {
    return this.getApplications(this.desktopApplicationsEndpoint);
  }
}
