import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { DropdownDatasource, FormFieldBuilder } from 'ngx-lib-formly';
import { CropDropdown } from '../../../dashboard/model/crop-dropdown.model';

@Component({
  selector: 'app-crops-dropdown',
  templateUrl: './crops-dropdown.component.html',
  styleUrls: ['./crops-dropdown.component.scss']
})
export class CropsDropdownComponent implements OnInit {
  @Input() options: any[];
  @Output() change: EventEmitter<any> = new EventEmitter();

  form = new FormGroup({});
  model: CropDropdown = {};
  fields: FormlyFieldConfig[];


  constructor() {
  }

  ngOnInit() {
    // const items: any[] = DropdownDatasource.automap(this.options, 'name');
    const items: any[] = this.options;
    this.model = { cropId: items[0].value };
    let b = new FormFieldBuilder<any>();

    b.basicSelectField(
      {
        key: 'cropId',
        id: 'field_crops',
        fieldName: 'Crops',
        cssClass: 'cropDropDown',
        removable: true,
        change: (item) => {
          this.changeCrop(item);
        }
      },
      items
    );
    this.fields = b.toArray();
    this.changeCrop(items[0]);
  }

  private changeCrop(crop) {
    this.change.emit(crop);
  }
}
