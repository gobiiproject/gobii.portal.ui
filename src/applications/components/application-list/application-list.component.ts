import { Component, Input } from '@angular/core';
import { App } from '../../../app/models/crop.model';

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.scss']
})
export class ApplicationListComponent {

  @Input() applications: App;
}
