import { Component, OnInit } from '@angular/core';
import { DropdownItem } from 'ngx-lib-formly/lib/models/dropdown-item.model';
import { Observable, of, Subscription } from 'rxjs';
import { CROPS } from '../../../app/constants/crops.constants';
import { App, Crop } from '../../../app/models/crop.model';
import { ApplicationService } from '../../service/application.service';

@Component({
  selector: 'app-web-application',
  templateUrl: './web-applications.component.html',
  styleUrls: ['./web-applications.component.scss']
})
export class WebApplicationsComponent implements OnInit {
  appList: App[];
  subscription = new Subscription();
  crops$: Observable<any[]>;

  constructor(
    private service: ApplicationService,
  ) { }

  ngOnInit() {
    this.loadCrops();
  }

  loadCrops() {
    //Todo: change it to service from constant
    // Add via the service when the api's are ready - sometimes mocks data behaves's differently when used via observables.
    this.crops$ = of(CROPS);
  }

  onChange(item: DropdownItem) {
    this.subscription.add(this.service.getWebApps()
      .subscribe((apps: Crop[]) => {
        const selected = apps.find(e => e.name === item.value);
        this.appList = selected.apps;
      }));
  }
}
