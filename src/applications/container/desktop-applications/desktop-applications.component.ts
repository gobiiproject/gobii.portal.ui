import { Component, OnInit } from '@angular/core';
import { App } from '../../../app/models/crop.model';
import { ApplicationService } from '../../service/application.service';

@Component({
  selector: 'app-desktop-application',
  templateUrl: './desktop-applications.component.html',
  styleUrls: ['./desktop-applications.component.scss']
})
export class DesktopApplicationsComponent implements OnInit {
  appList: App[];

  constructor(
    private service: ApplicationService,
  ) { }

  ngOnInit() {
    this.loadApplicatons();
  }

  loadApplicatons() {
    this.service.getDesktopApps()
    .subscribe((apps: App[]) => (this.appList = apps));
  }
}
