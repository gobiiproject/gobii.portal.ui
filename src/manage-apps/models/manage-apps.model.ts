export interface ApplicationManager {
    id: string;
    name: string;
    description: string;
    imgUrl: string;
    permission: string;
    links: Link[];
    docs: Documentation[];
}

export interface Documentation {
    id: string;
    name: string;
    url: string;
}

export interface Link {
    id: string;
    name: string;
    url: string;
}
