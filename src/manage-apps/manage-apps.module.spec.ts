import { ManageAppsModule } from './manage-apps.module';

describe('ManageAppsModule', () => {
  let manageAppsModule: ManageAppsModule;

  beforeEach(() => {
    manageAppsModule = new ManageAppsModule();
  });

  it('should create an instance', () => {
    expect(manageAppsModule).toBeTruthy();
  });
});
