import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../app/shared/shared.module';
import { ManageAppsComponent } from './containers/manage-apps/manage-apps.component';
import { ManageAppsRoutingModule } from './manage-apps-routing.module';
import { ManageAppItemComponent } from './containers/manage-app-item/manage-app-item.component';
import { ManageAppFormComponent } from './components/manage-app-form/manage-app-form.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ManageAppsRoutingModule
  ],
  declarations: [ManageAppsComponent, ManageAppItemComponent, ManageAppFormComponent]
})
export class ManageAppsModule { }
