import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { baseUrl } from './../../app/constants/path.constants';
import { ApplicationManager } from '../models/manage-apps.model';

@Injectable({
  providedIn: 'root'
})
export class ManageAppsService {

  api = baseUrl;
  cropsEndpoint = '../../assets/crops.json';
  manageAppEndpoint = './../../assets/manage-apps.json';

  constructor(private http: HttpClient) { }

  getApplications(): Observable<ApplicationManager[]> {
    return this.http.get(this.manageAppEndpoint).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getApplication(id: string) {
    return this.http.get(this.manageAppEndpoint).pipe(
      map((response: any) => response[ parseInt(id) - 1 ]),
      catchError((error: any) => throwError(error))
    );
  }

}
