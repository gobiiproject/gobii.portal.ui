import { DropdownItem } from 'ngx-lib-formly/lib/models/dropdown-item.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { DropdownDatasource, FormFieldBuilder } from 'ngx-lib-formly';
import { CROPS } from '../../../app/constants/crops.constants';
import { permissions } from './../../../app/constants/permissions.constants';


@Component({
  selector: 'app-manage-app-form',
  templateUrl: './manage-app-form.component.html',
  styleUrls: ['./manage-app-form.component.scss']
})
export class ManageAppFormComponent implements OnInit {

  @Input() model;
  fields: FormlyFieldConfig[];
  permissions: any[] = permissions;
  dynamicForm = new FormGroup({});
  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<any> = new EventEmitter();
  @Input() docs: any;

  constructor(private _fb: FormBuilder) {

  }
  ngOnInit() {
    this.constructForm();
     if (this.docs === undefined) {
      this.constructDynamicForm(0);
    } else {
      this.constructDynamicForm(this.docs.length);
      this.hydrateDocumentForm();

    }
  }

  constructForm() {
    let b = new FormFieldBuilder<any>();
    b.textField({
      key: 'name',
      id: 'field_name',
      fieldName: 'Name',
    });
    b.textAreaField({
      key: 'description',
      id: 'field_description',
      fieldName: 'Description',
    });
    b.basicSelectField(
      {
        key: 'permission',
        id: 'field_permission',
        fieldName: 'Permissions',
        removable: true,
      },
      permissions
    );
    b.fileUploadField({
      key: 'app_image',
      id: 'field_image_uplaod',
      fieldName: 'Upload new Icon',
      floatLabel: 'always',
      appearance: 'outline'
    });

    b.basicSelectField(
      {
        key: 'crop',
        id: 'field_crop',
        fieldName: 'Crops',
        multiValue: true,
        removable: false,
      },
      CROPS
    );
    b.textField({
      key: 'appUrl',
      id: 'field_app_url',
      fieldName: 'Application Url',
    });
    this.fields = b.toArray();
  }

  constructDynamicForm(num: number) {
    this.dynamicForm = this._fb.group({
      docs: this._fb.array([this.addDocumentGroup()])
    });
    for (let index = 0; index < num - 1; index++) {
      this.addDocLink();
    }
  }

  hydrateDocumentForm() {
    const newValue = this.createArrays();
    (<FormArray>this.dynamicForm.get('docs')).setValue(newValue);
  }

  createArrays() {
    const newValue = [];
    this.docs.forEach(element => {
      let name = '';
      let url = '';
      name = element.name;
      url = element.url;
      newValue.push({ name: name, url: url });
    });
    return newValue;
  }

  private addDocumentGroup(): FormGroup {
    return this._fb.group({
      name: '',
      url: ''
    });
  }
  addDocLink(): void {
    this.documentArray.push(this.addDocumentGroup());
  }

  removeDocLink(index: number): void {
    this.documentArray.removeAt(index);
  }

  get documentArray(): FormArray {
    return <FormArray>this.dynamicForm.get('docs');
  }

  onCancel() {
    this.cancel.emit();
  }

  onSaveForm() {
    const newModel = {...this.model, docs: this.dynamicForm.value};
    this.save.emit();
  }

}
