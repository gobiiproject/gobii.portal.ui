import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageAppsComponent } from './containers/manage-apps/manage-apps.component';
import { ManageAppItemComponent } from './containers/manage-app-item/manage-app-item.component';

const routes: Routes = [
  {
    path: '',
    component: ManageAppsComponent
  },
  {
    path: 'create',
    component: ManageAppItemComponent
  },
  {
    path: ':id',
    component: ManageAppItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageAppsRoutingModule { }
