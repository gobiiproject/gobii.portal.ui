import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationManager } from './../../models/manage-apps.model';
import { TableDefinitionFactory } from '../../../app/constants/table-definition-factory';
import { TableDefinition } from '../../../app/models/table-definition.model';
import { ManageAppsService } from '../../services/manage-apps.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-manage-apps',
  templateUrl: './manage-apps.component.html',
  styleUrls: ['./manage-apps.component.scss']
})
export class ManageAppsComponent implements OnInit {
  readonly tableDefinition: TableDefinition = TableDefinitionFactory.manageAppsList();
  apps$: Observable<ApplicationManager[]>;
  constructor(
     private router: Router,
     private route: ActivatedRoute,
     private service: ManageAppsService
     ) { }

  ngOnInit() {
    this.loadApplicationList();
  }

  loadApplicationList() {
    this.apps$ = this.service.getApplications();
  }

  onAdd() {
    this.router.navigate(['create'], {
      relativeTo: this.route,
    });
  }

  onItemClicked(app: ApplicationManager) {
    this.router.navigate([`${app.id}`], {
      relativeTo: this.route
    });
  }

}
