import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { ManageAppsService } from '../../services/manage-apps.service';


@Component({
  selector: 'app-manage-app-item',
  templateUrl: './manage-app-item.component.html',
  styleUrls: ['./manage-app-item.component.scss']
})
export class ManageAppItemComponent implements OnInit, OnDestroy {
  application: any;
  docs: any;
  subscription =  new Subscription();

  constructor(
    readonly route: ActivatedRoute,
    readonly service: ManageAppsService,
    ) { }

   ngOnInit() {
    if (!this.isCreateMode) {
      this.loadApplication();
    }
  }

  get isCreateMode() {
   return this.route.snapshot.params.id === undefined;
  }

  loadApplication() {
    const appId = this.route.snapshot.params.id;
    this.subscription.add(this.service.getApplication(appId).subscribe(app => {
      this.application = app;
      this.docs = app.docs;
    }));
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }


}
