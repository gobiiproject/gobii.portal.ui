import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageAppItemComponent } from './manage-app-item.component';

describe('ManageAppItemComponent', () => {
  let component: ManageAppItemComponent;
  let fixture: ComponentFixture<ManageAppItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageAppItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageAppItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
