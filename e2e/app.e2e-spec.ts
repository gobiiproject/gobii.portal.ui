import { element, by } from 'protractor';

describe('Feature page', () => {
  it('displays correct vine from store.', () => {
    expect(element(by.name('synchronous-vine')).getText())
      .toEqual('Vine loaded synchronously: T08.33-01-01a');
  });
});
