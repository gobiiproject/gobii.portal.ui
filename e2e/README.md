# System Tests

## Setting Up

First, check that you have protractor installed: `node_modules/protractor/bin/protractor --version`. To download the driver files, run `node_modules/protractor/bin/webdriver-manager update`.

## Running the Tests

Run the tests with `node_modules/protractor/bin/protractor protractor.conf.js`.
