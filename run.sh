#!/usr/bin/env sh
set -e
set -x

export DOLLAR='$'
echo $NODE_ENV

mkdir -p /var/www/logs/nginx
envsubst < /var/www/nginx.conf.template > /var/www/nginx.conf
envsubst < /var/www/env.js.template > /var/www/build/assets/env.js
nginx -g 'daemon off;' -c /var/www/nginx.conf
nginx -c /var/www/nginx.conf
